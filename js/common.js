$(document).ready(function() {


	$(function() {
		jcf.replaceAll();
	});

	jcf.setOptions('Select', {
		wrapNative: false,
		wrapNativeOnMobile: false,
		fakeDropInBody: false,
		maxVisibleItems: 5,
	});

	$('.gallery__item').magnificPopup({
		type: 'image'	
	});

	$('.characteristics__select-btn--top').click(function(e) {
		var $message = $('.characteristics__mnu-wrap');

		if ($message.css('display') != 'block') {
			$message.show();

			var firstClick = true;
			$(document).bind('click.myEvent', function(e) {
				if (!firstClick && $(e.target).closest('.characteristics__mnu-wrap').length == 0) {
					$message.hide();
					$(document).unbind('click.myEvent');
				}
				firstClick = false;
			});
		}

		e.preventDefault();
	});

	$('.slider-wrap').owlCarousel({
		items: 1,
		loop: true
	});

	$('.leaders-slider').owlCarousel({
		margin: 20,
		responsive: {
			0: {
				items: 1,
			},
			480: {
				items: 2,
			},
			992: {
				items: 4,
			}
		}
	});

	$('.product-gallery').owlCarousel({
		items: 1,
		loop: true,
		center: true,
		margin: 10, 
		nav: true,
		navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>" ]
	});


	$(function() {
		$('#up').click(function() {
			$('html, body').animate({scrollTop: 0},1000);
			return false;
		});

	});

	$(window).scroll(function() {
		if($(this).scrollTop() >= 0.0000001) {
			$('.header-topline, .header-inner, .header-navigation').hide();
			$('.header-small').addClass('on');
			$('.header-margin').addClass('on');

		}
		else{

			$('.header-topline, .header-inner, .header-navigation').show();
			$('.header-small').removeClass('on');
			$('.header-margin').removeClass('on');
		}
	});


	$('.toggle-mnu').click(function() {
		$(this).toggleClass('on');
		$('.mobile-nav').slideToggle(500);
		return false;
	});

	$('.brends__btn').click(function() {
		$('.brends-inner').toggleClass('open');
		$('.brends__btn').toggleClass('on');
	});

	$('.categories-btn').click(function() {
		$('.sidebar').toggleClass('on');
	});

	$('.catalogue__heading--second').click(function() {
		$('.footer__catalogue--second').toggleClass('on');
	});

	$('.catalogue__heading--third').click(function() {
		$('.footer__catalogue--third').toggleClass('on');
	});

	$( function() {//rangeslider
		$( ".filters-inner__price #slider-range" ).slider({
			range: true,
			min: 0,
			max: 500,
			values: [ 75, 300 ],
			slide: function( event, ui ) {
				$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			}
		});
		$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			" - $" + $( "#slider-range" ).slider( "values", 1 ) );
	} );

	$( function() {//rangeslider
		$( ".filters-inner__weight #slider-range" ).slider({
			range: true,
			min: 0,
			max: 500,
			values: [ 75, 300 ],
			slide: function( event, ui ) {
				$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			}
		});
		$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			" - $" + $( "#slider-range" ).slider( "values", 1 ) );
	} );


	//TABS

	$('.tabgroup > div').hide();
	$('.tabgroup > div:first-of-type').show();
	$('.tabs a').click(function(e){
		e.preventDefault();
		var $this = $(this),
		tabgroup = '#'+$this.parents('.tabs').data('tabgroup'),
		others = $this.closest('li').siblings().children('a'),
		target = $this.attr('href');
		others.removeClass('active');
		$this.addClass('active');
		$(tabgroup).children('div').hide();
		$(target).show();

	});

	$(".expand-btn").click(function(){
		$(".tabgroup div").toggleClass("on");
		$(".expand-btn__arrow").toggleClass("on")
	});
});

